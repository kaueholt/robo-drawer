import Layout from '@/views/layout/Layout'
import usuario from '@/modules/usuario/routes'
import company from '@/modules/company/routes'
import department from '@/modules/department/routes'

export default {
  path: '/usuarios',
  component: Layout,
  redirect: '/usuario/list',
  name: 'Geral Usuários',
  meta: { title: 'Pessoas', icon: 'user' },
  children: [
    ...usuario,
    ...department,
    ...company,
  ]
}
