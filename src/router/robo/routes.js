import Layout from '@/views/layout/Layout'
import point from '@/modules/point/routes'
import route from '@/modules/route/routes'
import path from '@/modules/path/routes'
// import routines from '@/modules/routines/routes'
import models from '@/modules/models/routes'
import robot from '@/modules/robot/routes'

export default {
  path: '/robo',
  component: Layout,
  redirect: '/path/list',
  name: 'Geral Robô',
  meta: { title: 'Robôs', icon: 'reddit' },
  children: [
    ...robot,
    ...path,
    ...route,
    ...point,
    ...models,
    // ...routines,
  ]
}
