import Vue from 'vue'
import Vuex from 'vuex'
// import routines from '@/modules/routines/store'
import app from './modules/app'
import company from '@/modules/company/store'
import department from '@/modules/department/store'
import device from '@/modules/device/store'
import deviceType from '@/modules/deviceType/store'
import historyRead from '@/modules/historyRead/store'
import measure from '@/modules/measure/store'
import models from '@/modules/models/store'
import path from '@/modules/path/store'
import point from '@/modules/point/store'
import product from '@/modules/product/store'
import robot from '@/modules/robot/store'
import route from '@/modules/route/store'
import user from './modules/user'
import usuario from '@/modules/usuario/store'

import getters from './getters'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

const store = new Vuex.Store({
  modules: {
    // routines,
    app,
    company,
    department,
    device,
    deviceType,
    historyRead,
    measure,
    models,
    path,
    point,
    product,
    robot,
    route,
    user,
    usuario
  },
  getters
  // plugins: [vuexLocal.plugin]
})

export default store
