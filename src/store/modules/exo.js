const exo = {
  state: {
    selected: {},
    list: [
      {
        id: '1',
        status: 1,
        model: 'XBR 19',
        horimetre: 19,
        lastUser: 'Pedro Silva',
        inspectionHours: 1
      },
      {
        id: '2',
        status: 0,
        model: 'XBR 19',
        horimetre: 42,
        lastUser: 'Arnaldo Silva',
        inspectionHours: 4
      },
      {
        id: '3',
        status: 1,
        model: 'XBR 22',
        horimetre: 21,
        lastUser: 'Maria Silva',
        inspectionHours: 2
      },
      {
        id: '4',
        status: 1,
        model: 'XBR 19',
        horimetre: 13,
        lastUser: 'Maria Silva',
        inspectionHours: 1
      }
    ]
  },
  mutations: {
    SELECT_EXO: (state, exo) => {
      state.selected = exo
    }
  },
  actions: {
    selectExo({ commit }, exo) {
      commit('SELECT_EXO', exo)
    }
  }
}

export default exo
