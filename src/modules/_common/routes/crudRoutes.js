import Layout from '@/views/layout/Layout'

const crudRoutes = (title, name, icon) => {
  return {
    path: `/${name}`,
    redirect: `/${name}_list`,
    component: Layout,
    meta: {
      title: title,
      icon: icon,
      roles: ['admin']
    },
    children: [
      {
        path: 'list',
        name: `${name}_list`,
        component: () => import(`@/modules/${name}/views/index`),
        meta: { title: title, noCache: true }
      },
      {
        path: 'new',
        name: `${name}_new`,
        hidden: true,
        component: () => import(`@/modules/${name}/views/upsert`),
        meta: { title: `Cadastro de ${title}`, noCache: true }
      },
      {
        path: ':id',
        name: `${name}_edit`,
        hidden: true,
        component: () => import(`@/modules/${name}/views/upsert`),
        meta: { title: `Editar ${title}`, noCache: true }
      }
    ]
  }
}

export { crudRoutes }

